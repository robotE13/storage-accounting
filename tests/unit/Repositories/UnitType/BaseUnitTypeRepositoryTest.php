<?php

namespace tests\unit\Repositories\UnitType;

use RobotE13\StorageAccounting\Entities\StorageUnitType\PredefinedUnitType;
use RobotE13\StorageAccounting\Repositories\StorageUnitType\UnitTypeRepository;

abstract class BaseUnitTypeRepositoryTest extends \Codeception\Test\Unit
{

    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var UnitTypeRepository
     * @specify
     */
    protected $repository;

    // tests
    public function testGet()
    {
        $this->specify("Getting predefined unit type", function(){
            $unitType = $this->repository->find('Default');
            expect('Type name is `Default`',$unitType->getName())->equals(PredefinedUnitType::TYPE_NAME);
        });
        $this->specify("Getting custom unit type", function(){
            $typeName = 'Book';
            $unitType = (new \Helper\Builders\UnitTypeBuilder())
                    ->withName($typeName)
                    ->create();
            $this->repository->add($unitType);
            $found = $this->repository->find($typeName);
            expect('Type name is `Book`',$unitType->getName())->equals($found->getName());
        });
    }

}
