<?php

namespace tests\unit\Repositories\UnitType;

use RobotE13\StorageAccounting\Entities\StorageUnitType\PredefinedUnitType;
use RobotE13\StorageAccounting\Repositories\StorageUnitType\MemoryUnitTypeRepository;

class InMemoryUnitTypeTest extends BaseUnitTypeRepositoryTest
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
        $this->repository = new MemoryUnitTypeRepository([
            'Default' => PredefinedUnitType::class
        ]);
    }

}
