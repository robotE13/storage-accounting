<?php

namespace Services;

use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use RobotE13\StorageAccounting\Entities\StorageUnit\StorageUnit;
use RobotE13\StorageAccounting\Repositories\StorageUnitType\MemoryUnitTypeRepository;
use RobotE13\StorageAccounting\Entities\StorageUnitType\PredefinedUnitType;
use RobotE13\StorageAccounting\Services\StorageUnit\Update\{
    UpdateStorageUnit,
    UpdateStorageUnitHandler,
    UpdateUnitAttribute,
    UpdateUnitAttributeHandler
};
use RobotE13\StorageAccounting\Services\StorageItem\Update\{
    UpdateItem,
    UpdateItemAttribute,
    UpdateItemHandler,
    UpdateItemAttributeHandler
};
use RobotE13\StorageAccounting\Services\StorageItem\AddSkuVariant\{
    AddSkuVariant,
    AddSkuVariantHandler
};
use RobotE13\StorageAccounting\Services\StorageItem\Create\{
    CreateItem,
    CreateItemHandler,
    CreateItemValidator
};
use RobotE13\StorageAccounting\Repositories\StorageUnit\{
    MemoryStorageItemRepository,
    MemoryStorageUnitRepository
};

class StorageItemCommandsTest extends \Codeception\Test\Unit
{

    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     *
     * @var \League\Tactician\CommandBus
     */
    protected $commandBus;

    /**
     * @var MemoryStorageItemRepository
     */
    protected $storageItems;

    /**
     * @var MemoryStorageUnitRepository
     */
    protected $storageUnits;

    protected function _before()
    {
        $unitTypes = new MemoryUnitTypeRepository(['Default' => PredefinedUnitType::class]);
        $this->storageItems = new MemoryStorageItemRepository();
        $this->storageUnits = new MemoryStorageUnitRepository($this->storageItems);

        $locator = new InMemoryLocator();
        $locator->addHandler(new CreateItemHandler($this->storageItems, $unitTypes), CreateItem::class);
        $locator->addHandler(new AddSkuVariantHandler($this->storageItems, $unitTypes), AddSkuVariant::class);
        $locator->addHandler(new UpdateItemAttributeHandler($this->storageItems), UpdateItemAttribute::class);
        $locator->addHandler(new UpdateItemHandler($this->storageItems), UpdateItem::class);
        $locator->addHandler(new UpdateUnitAttributeHandler($this->storageUnits), UpdateUnitAttribute::class);
        $locator->addHandler(new UpdateStorageUnitHandler($this->storageUnits), UpdateStorageUnit::class);
        $this->commandBus = new \League\Tactician\CommandBus([
            new \League\Tactician\Handler\CommandHandlerMiddleware(
                    new ClassNameExtractor(),
                    $locator,
                    new HandleInflector()
            ),
            new CreateItemValidator($this->storageItems, $this->storageUnits)
        ]);
    }

    public function testCreate()
    {
        $result = $this->createNewItem();
        expect("Item successfully created", $result->isSuccessful())->true();
        /* @var $created \RobotE13\StorageAccounting\Entities\StorageUnit\StorageItem */
        $created = $result->getResult();
        expect("Item has default SKU variant `PRO-12-061`", $created->getDefaultVariant()->getSkuNumber())->equals('PRO-12-061');
        expect("Item has title `Product1`", $created->getTitle())->equals('Product1');
        expect("Item has slug `test-product`", $created->getSlug())->equals('test-product');
        expect("Default variant cost `35.00`", $created->getDefaultVariant()->getPrice())->equals(35.00);
    }

    public function testAddSkuVariant()
    {

        $this->createNewItem();
        /* @var $result \RobotE13\StorageAccounting\Services\CommandResult */
        $createdItem = $this->storageItems->findBySlug('test-product');
        $command = new AddSkuVariant($createdItem->getUid()->getString(),
                'PRO-12-062', 'Sku title', 'Default', StorageUnit::STATUS_ACTIVE);
        $result = $this->commandBus->handle($command);
        expect('Command was successfully executed', $result->isSuccessful())->true();
        $foundedItem = $this->storageItems->findBySlug('test-product');
        expect('Loaded storage item has 2 variants (default, and added)', $foundedItem->getVariants()->getAll())->count(2);
        expect('Default variant price is 35.00', $foundedItem->getDefaultVariant()->getPrice())->equals(35.00);
    }

    public function testUpdateItemAttribute()
    {
        $this->createNewItem();

        $this->specify('Test change title', function () {
            $item = $this->storageItems->findBySlug('test-product');
            $command = new UpdateItemAttribute($item->getUid()->getString(), 'title', 'New__Value');
            $this->commandBus->handle($command);
            $foundedItem = $this->storageItems->findBySlug('test-product');
            expect('Item has title: "New__Value"', $foundedItem->getTitle())->equals('New__Value');
        });

        $this->specify('Test fail change slug with illegal characters.', function () {
            $item = $this->storageItems->findBySlug('test-product');
            $command = new UpdateItemAttribute($item->getUid()->getString(), 'slug', 'NewValue???!A');
            expect('Item has title: "New__Value"', fn() => $this->commandBus->handle($command))->throws(\InvalidArgumentException::class);
        });

        $this->specify('Test change unit price', function () {
            $item = $this->storageItems->findBySlug('test-product');
            $command = new UpdateUnitAttribute($item->getDefaultVariant()->getSkuNumber(), 'price', '45.65');
            $this->commandBus->handle($command);
            $foundedItem = $this->storageItems->findBySlug('test-product');
            expect('Default variant price was changed to: 45.65', $foundedItem->getDefaultVariant()->getPrice())->equals('45.65');
        });

        $this->specify('Test change unit characteristic (volume)', function () {
            $item = $this->storageItems->findBySlug('test-product');
            $command = new UpdateUnitAttribute($item->getDefaultVariant()->getSkuNumber(), 'volume', '121ml');
            $this->commandBus->handle($command);
            $foundedItem = $this->storageItems->findBySlug('test-product');
            expect('Volume was changed to: 121ml', $foundedItem->getDefaultVariant()->getCharacteristic('volume'))->equals('121ml');
        });
    }

    public function testBatchUpdateAttributes()
    {
        $this->createNewItem();
        $this->specify('Succassfull update item', function () {
            $item = $this->storageItems->findBySlug('test-product');
            $command = new UpdateItem($item->getUid()->getString(), ['title' => 'New__Value', 'description' => 'New_Item_Description', 'slug' => 'new-slug']);
            $this->commandBus->handle($command);
            $foundedItem = $this->storageItems->findBySlug('new-slug');
            expect('Item has title: "New__Value"', $foundedItem->getTitle())->equals('New__Value');
            expect('Item has description: "New_Item_Description"', $foundedItem->getDescription())->equals('New_Item_Description');
        });

        $this->specify('Fail to update with incorrect attribute name', function () {
            $item = $this->storageItems->findBySlug('new-slug');
            $command = new UpdateItem($item->getUid()->getString(), ['attrNotExisted' => 'New__Value']);
            expect('Invalid argument (attribute not exist) throwed',
                            fn() => $this->commandBus->handle($command))
                    ->throws(\InvalidArgumentException::class);
        });

        $this->specify('Succassfull update stoarge unit', function () {
            $item = $this->storageItems->findBySlug('new-slug');
            $command = new UpdateStorageUnit($item->getDefaultVariant()->getSkuNumber(),
                    ['price'=>'33.99','title'=>'New_Unit_Title'],
                    ['volume'=>'111ml']
            );
            $this->commandBus->handle($command);
            $updatedUnit = $this->storageItems->findBySlug('new-slug')->getDefaultVariant();
            expect('Default variant price was changed to: 33.99', $updatedUnit->getPrice())->equals('33.99');
            expect('Default variant title was changed to: New_Unit_Title', $updatedUnit->getTitle())->equals('New_Unit_Title');
            expect('Volume was changed to: 111ml', $updatedUnit->getCharacteristic('volume'))->equals('111ml');
        });
    }

    protected function createNewItem(): \RobotE13\StorageAccounting\Services\CommandResult
    {
        $command = new CreateItem(
                "Product1",
                "test-product",
                "PRO-12-061",
                "Default",
                StorageUnit::STATUS_ACTIVE,
                'Item description',
                '',
                35.00,
                ['volume' => '135ml']
        );
        return $this->commandBus->handle($command);
    }

}
