<?php

namespace Entities;

use RobotE13\StorageAccounting\Entities\StorageUnitType\PredefinedUnitType;

class StorageUnitTypeTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;
    private $typeBuilder;

    protected function _before()
    {
        $this->typeBuilder = new \Helper\Builders\UnitTypeBuilder();
    }

    // tests
    public function testCreatePredefined()
    {
        $type = new PredefinedUnitType();
        expect('Название типа хранимого объекта `Default`', $type->getName())->equals(PredefinedUnitType::TYPE_NAME);
    }

    public function testCreateCustom()
    {
        $type = $this->typeBuilder->withName('Book')->create();
        expect('Название типа хранимого объекта `Book`', $type->getName())->equals('Book');
    }

}
