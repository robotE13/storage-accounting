<?php

namespace Entities;

use Helper\Builders\StorageUnitBuilder;

class StorageUnitTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    // tests
    public function testSuccessfulCreate()
    {
        $bookProduct = (new StorageUnitBuilder())->withSkuNumber('TestSKU01')->create();
        expect('Товар имеет артикул `TestSKU01`',$bookProduct->getSkuNumber())->equals('TestSKU01');

    }

}
