<?php

namespace Entities;

use Helper\Builders\StorageItemBuilder;

class StorageItemTest extends \Codeception\Test\Unit
{

    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @dataProvider attributeProvider
     */
    public function testUpdateAttribute($attribute,$newValue)
    {
        $product = (new StorageItemBuilder())->create();
        $product->updateAttribute($attribute, $newValue);
        $getter = "get". ucfirst($attribute);
        expect('New attribute value', $product->{$getter}())->equals($newValue);
    }

    /**
     * @return array
     */
    public function attributeProvider()
    {
        return [
            ['attribute' => "brief", 'value' => "NEW Brief"],
            ['attribute' => "title", 'value' => "NEW Title"],
        ];
    }

}
