<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace Helper\Builders;

/**
 * Description of KeepingUnitBuilder
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UnitTypeBuilder
{

    private $name;
    private $parameters = [];
    private $measureUnit;

    public function __construct()
    {
        $this->name = 'Common';
        $this->measureUnit = new \RobotE13\StorageAccounting\Entities\MeasureUnit('kilogramm', 'kg.');
    }

    public function withName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function create()
    {
        return new \RobotE13\StorageAccounting\Entities\StorageUnitType\CustomUnitType(
                $this->name,
                $this->parameters,
                $this->measureUnit
        );
    }

}
