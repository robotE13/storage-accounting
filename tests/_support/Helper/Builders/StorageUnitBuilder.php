<?php

namespace Helper\Builders;

use RobotE13\StorageAccounting\Entities\StorageUnit\StorageUnit;

/**
 * Description of UserBuilder
 *
 * @author robotR13
 */
class StorageUnitBuilder
{

    private $skuNumber;
    private $title;
    private $skuType;
    private $skuStatus;
    private $characteristics;

    public function __construct()
    {
        $this->skuNumber = "TEST-01";
        $this->title = "Example Item";
        $this->skuType = (new UnitTypeBuilder())->create();
        $this->skuStatus = 1;
    }

    /**
     * Создать вариант с артикулом.
     * @param string $skuNumber
     * @return $this
     */
    public function withSkuNumber(string $skuNumber)
    {
        $this->skuNumber = $skuNumber;
        return $this;
    }

    public function withTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function withPluggableType(\RobotE13\StorageAccounting\Entities\StorageUnitType\AbstractUnitType $type)
    {
        $this->skuType = $type;
        return $this;
    }

    public function create(): StorageUnit
    {
        $storageUnit = new StorageUnit(
                $this->skuNumber,
                $this->title,
                $this->skuType,
                $this->skuStatus
//                $characteristics,
//                $preview,
//                $price,
//                $availableQuantity
        );

        return $storageUnit;
    }

}
