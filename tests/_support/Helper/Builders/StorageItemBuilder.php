<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Helper\Builders;

use RobotE13\StorageAccounting\Entities\StorageUnit\StorageItem;

/**
 * Description of StorageItemBuilder
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageItemBuilder
{

    /**
     *
     * @var StorageUnitBuilder
     */
    private $unitBuilder;

    public function __construct()
    {
        $this->unitBuilder = new StorageUnitBuilder();
    }

    public function create(): StorageItem
    {
        $storageItem = new StorageItem(new \RobotE13\StorageAccounting\Entities\Id(),
                'title', 'slug', 'desc', [$this->unitBuilder->create()]);

        return $storageItem;
    }

}
