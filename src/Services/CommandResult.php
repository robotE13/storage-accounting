<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services;

use Webmozart\Assert\Assert;

/**
 * Description of CommandResult
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class CommandResult
{

    /**
     * @var mixed
     */
    private $result;

    /**
     * @var array
     */
    private $errors = [];

    public function __construct($result = false, array $errors = [])
    {
        if($result === false)
        {
            Assert::notEmpty($errors, "If there is no result of the command execution (ended with errors), you must pass an array with errors.");
        }else{
            Assert::isEmpty($errors,"If the command is successfully executed, the error array must be empty.");
        }
        $this->result = $result;
        $this->errors = $errors;
    }

    public function getResult()
    {
        return $this->result;
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    public function getFirstError()
    {
        return !$this->isSuccessful() ? reset($this->errors)[0] : '';
    }

    public function addError($key, string $error)
    {
        $this->result = false;
        $this->errors[$key][] = $error;
    }

    public function isSuccessful()
    {
        return $this->result !== false;
    }

}
