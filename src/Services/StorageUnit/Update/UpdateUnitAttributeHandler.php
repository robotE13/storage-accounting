<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageUnit\Update;

use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageUnitRepository;

/**
 * Description of UpdateStorageUnitHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateUnitAttributeHandler
{
    /**
     * @var StorageUnitRepository
     */
    private $units;

    public function __construct(StorageUnitRepository $units)
    {
        $this->units = $units;
    }

    public function handle(UpdateUnitAttribute $command)
    {
        $unit = $this->units->find($command->getSkuNumber());
        $unit->updateAttribute($command->getAttribute(), $command->getValue());
        $this->units->update($unit);
    }

}
