<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageUnit\Update;

/**
 * Description of UpdateStorageUnit
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateUnitAttribute
{

    /**
     * @var string
     */
    protected $skuNumber;

    /**
     * Updated attribute name.
     * @var string
     */
    protected $attribute;

    /**
     * @var string
     */
    protected $value;

    public function __construct(string $skuNumber, string $attribute, string $value)
    {
        $this->skuNumber = $skuNumber;
        $this->attribute = $attribute;
        $this->value = $value;
    }

    public function getSkuNumber(): string
    {
        return $this->skuNumber;
    }

    public function getAttribute(): string
    {
        return $this->attribute;
    }

    public function getValue(): string
    {
        return $this->value;
    }

}
