<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageUnit\Update;

/**
 * Description of UpdateStorageUnit
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateStorageUnit
{

    /**
     * @var string
     */
    private $skuNumber;

    /**
     * @var array новые значения для для атрибутов. Ассоциативный массив ключ => значение
     */
    private $attributes;

    /**
     * @var array of unit type dynamic characteristics
     */
    private $characteristics;

    public function __construct(string $skuNumber, array $attributes, array $characteristics = [])
    {
        $this->skuNumber = $skuNumber;
        $this->attributes = $attributes;
        $this->characteristics = $characteristics;
    }

    public function getSkuNumber(): string
    {
        return $this->skuNumber;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getCharacteristics(): array
    {
        return $this->characteristics;
    }

}
