<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Update;

use RobotE13\StorageAccounting\Services\CommandResult;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemRepository;

/**
 * Description of UpdateItemDescriptionHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateItemAttributeHandler
{
    /**
     * @var StorageItemRepository
     */
    private $storageItems;

    public function __construct(StorageItemRepository $storageItems)
    {
        $this->storageItems = $storageItems;
    }

    public function handle(UpdateItemAttribute $command)
    {
        $item = $this->storageItems->findById($command->getUid());
        $item->updateAttribute($command->getAttribute(), $command->getValue());
        $this->storageItems->update($item);
        return new CommandResult($item);
    }
}
