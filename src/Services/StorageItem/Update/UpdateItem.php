<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Update;

/**
 * Description of UpdateItem
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateItem
{

    /**
     * @var string
     */
    protected $uid;

    /**
     * @var array новые значения для для атрибутов. Ассоциативный массив ключ => значение
     */
    private $attributes = [];

    public function __construct(string $uid, array $attributes)
    {
        $this->uid = $uid;
        $this->attributes = $attributes;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

}
