<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Update;

use RobotE13\StorageAccounting\Services\CommandResult;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemRepository;

/**
 * Description of UpdateItemHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateItemHandler
{

    /**
     * @var StorageItemRepository
     */
    private $storageItems;

    public function __construct(StorageItemRepository $storageItems)
    {
        $this->storageItems = $storageItems;
    }

    public function handle(UpdateItem $command)
    {
        $item = $this->storageItems->findById($command->getUid());
        foreach ($command->getAttributes() as $attribute => $value)
        {
            $item->updateAttribute($attribute, $value);
        }
        $this->storageItems->update($item);
        return new CommandResult($item);
    }

}
