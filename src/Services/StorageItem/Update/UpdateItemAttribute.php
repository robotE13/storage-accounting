<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Update;

/**
 * Description of UpdateItemDescription
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UpdateItemAttribute
{

    /**
     * @var string
     */
    protected $uid;

    /**
     * Updated attribute name.
     * @var string
     */
    protected $attribute;

    /**
     * @var string
     */
    protected $value;

    public function __construct(string $uid, string $attribute, string $value)
    {
        $this->uid = $uid;
        $this->attribute = $attribute;
        $this->value = $value;
    }

    public function getUid(): string
    {
        return $this->uid;
    }

    public function getAttribute(): string
    {
        return $this->attribute;
    }

    public function getValue(): string
    {
        return $this->value;
    }

}
