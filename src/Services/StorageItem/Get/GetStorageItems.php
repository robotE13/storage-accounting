<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Get;

use RobotE13\StorageAccounting\Entities\Id;

/**
 * Description of GetStorageItems
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class GetStorageItems
{

    /**
     * @var []
     */
    protected $uids;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $skuNumber;

    /**
     *
     * @var int the number of records to be fetched in each batch.
     */
    protected $batchSize;

    /**
     * Параметры по которым будет фильтроваться набор.
     * @param array $uids one or more Storage Item UUID's to apply to the filter
     * @param string $title storage item title
     * @param string $slug human-readable url
     * @param string $skuNumber storage keeping unit number
     * @param int $batchSize the number of records to be fetched in each batch
     */
    public function __construct(array $uids = [], string $title = '', string $slug = '', string $skuNumber = '', int $batchSize = 20)
    {
        $this->uids = array_map(fn($uid) => (new Id($uid))->getBytes(), $uids);
        $this->title = $title;
        $this->slug = $slug;
        $this->skuNumber = $skuNumber;
        $this->batchSize = $batchSize;
    }

    public function getUid()
    {
        return $this->uids;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function getSkuNumber()
    {
        return $this->skuNumber;
    }

    public function getBatchSize(): int
    {
        return $this->batchSize;
    }

}
