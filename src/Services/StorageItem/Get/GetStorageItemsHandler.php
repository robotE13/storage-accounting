<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Get;

use RobotE13\StorageAccounting\Services\CommandResult;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemReadRepository;

/**
 * Description of GetStorageItemsHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class GetStorageItemsHandler
{

    /**
     * @var StorageItemReadRepository
     */
    private $storageItems;

    public function __construct(StorageItemReadRepository $storageItems)
    {
        $this->storageItems = $storageItems;
    }

    public function handle(GetStorageItems $command): CommandResult
    {
        return new CommandResult($this->storageItems->getPagination($command));
    }

}
