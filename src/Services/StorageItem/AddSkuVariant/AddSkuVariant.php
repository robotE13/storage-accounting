<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\AddSkuVariant;

/**
 * Description of AddSku
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
final class AddSkuVariant
{

    private $storageItemUid;
    private $number;
    private $title;
    private $preview;
    private $type;

    /**
     * @var integer
     */
    private $status;
    private $characteristics = [];
    private $price;

    public function __construct(string $storageItemUid, $number, $title, $type, int$status, $characteristics = [], $preview = '', float $price = 0.00)
    {
        $this->storageItemUid = $storageItemUid;
        $this->number = $number;
        $this->title = $title;
        $this->preview = $preview;
        $this->type = $type;
        $this->status = $status;
        $this->characteristics = $characteristics;
        $this->price = $price;
    }

    /**
     *
     * @return string Item id to which we add the variant of the storage keeping unit
     */
    public function getStorageItemUid(): string
    {
        return $this->storageItemUid;
    }

    /**
     *
     * @return string storage unit unique number (SKU number)
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    public function getStatus():int
    {
        return $this->status;
    }

    /**
     * @return string storage unit title
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string url to item image
     */
    public function getPreview(): string
    {
        return $this->preview;
    }

    /**
     * @return string SKU type name
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getCharacteristics(): array
    {
        return $this->characteristics;
    }

    /**
     * @return float item cost
     */
    public function getPrice(): float
    {
        return $this->price;
    }

}
