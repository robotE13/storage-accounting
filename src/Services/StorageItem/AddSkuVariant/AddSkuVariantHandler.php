<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\AddSkuVariant;

use RobotE13\StorageAccounting\Entities\StorageUnit\StorageUnit;
use RobotE13\StorageAccounting\Services\CommandResult;
use RobotE13\StorageAccounting\Repositories\{
    StorageUnit\StorageItemRepository,
    StorageUnitType\UnitTypeRepository
};

/**
 * Description of AddSkuVariantHndler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class AddSkuVariantHandler
{

    /**
     * @var StorageItemRepository
     */
    private $storageItems;

    /**
     * @var UnitTypeRepository
     */
    private $unitTypes;

    public function __construct(StorageItemRepository $storageItems, UnitTypeRepository $unitTypes)
    {
        $this->storageItems = $storageItems;
        $this->unitTypes = $unitTypes;
    }

    public function handle(AddSkuVariant $command)
    {
        $unit = new StorageUnit(
                $command->getNumber(),
                $command->getTitle(),
                $this->unitTypes->find($command->getType()),
                $command->getStatus(),
                $command->getCharacteristics(),
                $command->getPreview(),
                $command->getPrice(),
                //$availableQuantity
        );
        $item = $this->storageItems->findById($command->getStorageItemUid());
        $item->addVariant($unit);
        $this->storageItems->update($item);
        return new CommandResult(true);
    }

}
