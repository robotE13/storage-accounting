<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Create;

use RobotE13\StorageAccounting\Services\CommandResult;
use RobotE13\StorageAccounting\Repositories\StorageUnit\{
    StorageItemRepository,
    StorageUnitRepository
};

/**
 * Description of CreateItemValidator
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class CreateItemValidator implements \League\Tactician\Middleware
{

    /**
     * @var StorageItemRepository
     */
    private $storageItems;

    /**
     * @var StorageUnitRepository
     */
    private $storageUnits;

    public function __construct(StorageItemRepository $storageItems, StorageUnitRepository $storageUnits)
    {
        $this->storageItems = $storageItems;
        $this->storageUnits = $storageUnits;
    }

    /**
     *
     * @param CreateItem $command
     * @param callable $next
     * @return CommandResult
     */
    public function execute($command, callable $next)
    {
        if(get_class($command) === CreateItem::class)
        {
            $errors = [];
            if($this->storageItems->existsBySlug($command->getSlug()))
            {
                $errors['slug'][] = 'Slug ' . $command->getSlug() . ' already been assigned.';
            }
            if($this->storageUnits->exists($command->getSkuNumber()))
            {
                $errors['number'][] = 'SKU number ' . $command->getSkuNumber() . ' already been assigned.';
            }
            if(!empty($errors))
            {
                return new CommandResult(false, $errors);
            }
        }
        return $next($command);
    }

}
