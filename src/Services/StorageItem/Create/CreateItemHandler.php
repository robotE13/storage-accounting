<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Create;

use RobotE13\StorageAccounting\Services\CommandResult;
use RobotE13\StorageAccounting\Repositories\StorageUnit\StorageItemRepository;
use RobotE13\StorageAccounting\Repositories\StorageUnitType\UnitTypeRepository;
use RobotE13\StorageAccounting\Entities\{
    Id,
    StorageUnit\StorageUnit,
    StorageUnit\StorageItem
};

/**
 * Description of CreateItemHandler
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class CreateItemHandler
{

    /**
     * @var StorageItemRepository
     */
    private $storageItems;

    /**
     * @var UnitTypeRepository
     */
    private $unitTypes;

    public function __construct(StorageItemRepository $storageItems, UnitTypeRepository $unitTypes)
    {
        $this->storageItems = $storageItems;
        $this->unitTypes = $unitTypes;
    }

    public function handle(CreateItem $command)
    {
        $variants = [
            new StorageUnit(
                    $command->getSkuNumber(),
                    $command->getTitle(),
                    $this->unitTypes->find($command->getSkuTypeName()),
                    $command->getSkuStatus(),
                    $command->getCharacteristics(),
                    $command->getPreview(),
                    $command->getPrice()
//                $availableQuantity
        )];
        $item = new StorageItem(
                new Id(),
                $command->getTitle(),
                $command->getSlug(),
                $command->getDescription(),
                $variants
//                $defaultVariant
        );
        $this->storageItems->add($item);
        return new CommandResult($item);
    }

}
