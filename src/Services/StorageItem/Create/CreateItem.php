<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Services\StorageItem\Create;

/**
 * Create item command.
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class CreateItem
{

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $skuNumber;

    /**
     * @var string
     */
    private $skuTypeName;

    /**
     * @var int
     */
    private $skuStatus;

    /**
     * @var string
     */
    private $preview;

    /**
     * @var string
     */
    private $price;

    /**
     * @var array
     */
    private $characteristics;

    public function __construct(string $title, string $slug, string $skuNumber, string $skuTypeName, int $skuStatus, string $description = '', string $preview = '', float $price = 0.00, array $characteristics = [])
    {
        $this->title = $title;
        $this->slug = $slug;
        $this->description = $description;
        $this->skuNumber = $skuNumber;
        $this->skuTypeName = $skuTypeName;
        $this->skuStatus = $skuStatus;
        $this->preview = $preview;
        $this->price = $price;
        $this->characteristics = $characteristics;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getSkuNumber(): string
    {
        return $this->skuNumber;
    }

    public function getSkuTypeName(): string
    {
        return $this->skuTypeName;
    }

    public function getSkuStatus(): int
    {
        return $this->skuStatus;
    }

    public function getPreview(): string
    {
        return $this->preview;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getCharacteristics(): array
    {
        return $this->characteristics;
    }

}
