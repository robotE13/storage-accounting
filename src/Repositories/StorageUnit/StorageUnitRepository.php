<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Repositories\StorageUnit;

use RobotE13\StorageAccounting\Entities\StorageUnit\StorageUnit;

/**
 * Description of StorageUnitRepository
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface StorageUnitRepository
{

    /**
     *
     * @param string $skuNumber SKU number
     * @return AbstractStorageUnit
     */
    public function find($skuNumber): StorageUnit;

    public function update(StorageUnit $unit);

    /**
     *
     * @param string $name keeping unit name
     */
    public function remove($name);

    /**
     * @param string $skuNumber
     * @return bool
     */
    public function exists($skuNumber): bool;
}
