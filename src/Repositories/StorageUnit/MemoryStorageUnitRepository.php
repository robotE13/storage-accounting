<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Repositories\StorageUnit;

use RobotE13\StorageAccounting\Entities\StorageUnit\StorageUnit;


/**
 * Description of MemoryStorageUnitRepository
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class MemoryStorageUnitRepository implements StorageUnitRepository
{
    /**
     *
     * @var StorageItemRepository
     */
    private $items;

    public function __construct(StorageItemRepository &$items)
    {
        $this->items = $items;
    }

    public function exists($skuNumber): bool
    {

        return key_exists($skuNumber, $this->storageUnits);
    }

    public function find($skuNumber): StorageUnit
    {
        $item = $this->items->findBySku($skuNumber);
        return $item->getVariants()->get($skuNumber);
    }

    public function remove($name)
    {

    }

    public function update(StorageUnit $unit)
    {
        $item = $this->items->findBySku($unit->getSkuNumber());
        $item->getVariants()->remove($unit->getSkuNumber());
        $item->getVariants()->add($unit);
        $this->items->update($item);
    }

}
