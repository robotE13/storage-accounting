<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Repositories\StorageUnit;

use Webmozart\Assert\Assert;
use RobotE13\StorageAccounting\Repositories\NotFoundException;
use RobotE13\StorageAccounting\Entities\StorageUnit\StorageItem;

/**
 * Description of MemoryStorageItemRepository
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class MemoryStorageItemRepository implements StorageItemRepository
{

    /**
     *
     * @var StorageItem[]
     */
    private $items = [];

    public function add(StorageItem $item)
    {
        Assert::keyNotExists($this->items, $item->getUid()->getString());
        $this->items[$item->getUid()->getString()] = $item;
    }

    public function findById($uid): StorageItem
    {
        if(!$this->existsById($uid))
        {
            throw new NotFoundException("Storage item not found.");
        }
        return clone $this->items[$uid];
    }

    public function findBySlug($slug): StorageItem
    {
        foreach ($this->items as $uid => $item)
        {
            if($item->getSlug() === $slug)
            {
                return clone $this->items[$uid];
            }
        }
        throw new NotFoundException("Storage item not found.");
    }

    public function findBySku($skuNumber): StorageItem
    {
        foreach ($this->items as $uid => $item)
        {
            if($item->getVariants()->get($skuNumber))
            {
                return clone $item;
            }
        }
        throw new NotFoundException("Storage item not found.");
    }

    public function remove($uid): StorageItem
    {
        if(!$this->existsById($uid))
        {
            throw new NotFoundException("Storage item not found.");
        }
        $item = $this->items[$uid];
        unset($this->items[$uid]);
        return $item;
    }

    public function update(StorageItem $item)
    {
        $this->items[$item->getUid()->getString()] = $item;
    }

    public function existsById($uid): bool
    {
        return key_exists($uid, $this->items);
    }

    public function existsBySlug($slug): bool
    {
        foreach ($this->items as $item)
        {
            if($item->getSlug() === $slug)
            {
                return true;
            }
        }
        return false;
    }

}
