<?php

/*
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Repositories\StorageUnit;

use RobotE13\StorageAccounting\Repositories\NotFoundException;
use RobotE13\StorageAccounting\Entities\StorageUnit\StorageItem;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface StorageItemRepository
{

    /**
     *
     * @param string $uid storage item ID
     * @return StorageItem
     * @throws NotFoundException
     */
    public function findById($uid): StorageItem;

    /**
     *
     * @param string $slug storage item slug for human understandable url
     * @return StorageItem
     * @throws NotFoundException
     */
    public function findBySlug($slug): StorageItem;

    /**
     *
     * @param string $skuNumber number of storage unit that belongs to this storage item
     * @return StorageItem
     * @throws NotFoundException
     */
    public function findBySku($skuNumber): StorageItem;

    /**
     *
     * @param StorageItem $item
     */
    public function add(StorageItem $item);

    /**
     *
     * @param AbstractStorageUnit $item
     */
    public function update(StorageItem $item);

    /**
     *
     * @param string $uid storage item uid
     */
    public function remove($uid): StorageItem;

    /**
     *
     * @param type $uid
     * @return bool
     */
    public function existsById($uid): bool;

    /**
     *
     * @param type $slug
     * @return bool
     */
    public function existsBySlug($slug): bool;
}
