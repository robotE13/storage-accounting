<?php

/*
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Repositories\StorageUnitType;

use RobotE13\StorageAccounting\Repositories\NotFoundException;
use RobotE13\StorageAccounting\Entities\StorageUnitType\AbstractUnitType;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface UnitTypeRepository
{

    /**
     *
     * @param string $typeName unit type name
     * @return AbstractUnitType
     * @throws NotFoundException
     */
    public function find($typeName): AbstractUnitType;

    /**
     *
     * @param AbstractUnitType $type
     * @return void
     */
    public function add(AbstractUnitType $type):void;
}
