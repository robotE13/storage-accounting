<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Repositories\StorageUnitType;

use RobotE13\StorageAccounting\Repositories\NotFoundException;
use RobotE13\StorageAccounting\Entities\StorageUnitType\AbstractUnitType;

/**
 * MemoryUnitTypeRepository
 *
 * Simple repository for test purposes.
 * Stored objects at internal array.
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class MemoryUnitTypeRepository implements UnitTypeRepository
{

    private $types = [];
    private $predefinedTypesDefinitions = [];

    public function __construct(array $predefinedTypesDefinitions = [])
    {
        $this->predefinedTypesDefinitions = $predefinedTypesDefinitions;
    }

    public function find($typeName): AbstractUnitType
    {
        if(!(key_exists($typeName,$this->predefinedTypesDefinitions) || key_exists($typeName, $this->types)))
        {
            throw new NotFoundException("Unit type not found.");
        }
        return $this->tryPredefined($typeName) ?? clone $this->types[$typeName];
    }

    public function add(AbstractUnitType $type): void
    {
        $this->types[$type->getName()] = $type;
    }

    protected function tryPredefined($typeName)
    {
        return key_exists($typeName, $this->predefinedTypesDefinitions) ? new $this->predefinedTypesDefinitions[$typeName]:null;
    }

}
