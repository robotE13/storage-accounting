<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities;

use Webmozart\Assert\Assert;

/**
 * Description of MeasureUnit
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class MeasureUnit
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $shortName;

    public function __construct(string $name, string $shortName)
    {
        Assert::regex($name,'/^[a-z]+$/','Only lowercase latin characters allowed');
        Assert::regex($shortName,'/^[a-z\.\-]+$/','Only lowercase latin characters, dot and dash symbols allowed.');
        $this->name = $name;
        $this->shortName = $shortName;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getShortName(): string
    {
        return $this->shortName;
    }

}
