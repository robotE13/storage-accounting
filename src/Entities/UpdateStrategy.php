<?php

/*
 * This file is part of the storage-accounting.
 *
 * Copyright 2021 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface UpdateStrategy
{
    /**
     *
     * @param mixed $value
     */
    public function execute($value);
}
