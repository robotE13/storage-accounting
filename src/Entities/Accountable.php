<?php

/*
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface Accountable extends Measurable
{

    /**
     *
     * @return string
     */
    public function getSkuNumber(): string;

    public function getAvailableQuantity();
}
