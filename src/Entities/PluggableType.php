<?php

/*
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
interface PluggableType
{

    /**
     * Возвращает имя модели данных.
     */
    public function getName(): string;

    /**
     * Returns additional attributes allowed to this type.
     * @return []
     */
    public function getParameters(): array;

    /**
     * Проверяет имеет ли эта модель данных указанный атрибут.
     * @param string $parameter parameter name
     * @return bool
     */
    public function hasParameter(string $parameter):bool;
}
