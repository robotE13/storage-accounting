<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities;

use Webmozart\Assert\Assert;

/**
 * Description of Collection
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
abstract class Collection
{

    private $items = [];

    public function __construct(array $items = [])
    {
        foreach ($items as $item)
        {
            $this->add($item);
        }
    }

    public function add($item): void
    {
        Assert::isInstanceOf($item, $this->getItemClass());
        if(key_exists($this->getIndex($item), $this->items))
        {
            $itemName = ucfirst($this->getItemName());
            throw new \DomainException("{$itemName} already exists.");
        }
        $this->items[$this->getIndex($item)] = $item;
    }

    /**
     *
     * @param mixed $index
     * @return mixed
     */
    public function get($index)
    {
        if($this->has($index))
        {
            return $this->items[$index];
        }
        throw new \DomainException("Variant sku with number {$index} not exist.");
    }

    public function remove($index)
    {
        if(!$this->has($index))
        {
            $itemName = ucfirst($this->getItemName());
            throw new \InvalidArgumentException("{$itemName} with index %s not found.");
        }

        $item = $this->items[$index];
        unset($this->items[$index]);
        return $item;
    }

    public function has($index)
    {
        return key_exists($index, $this->items);
    }

    public function getAll(): array
    {
        return $this->items;
    }

    /**
     * Имя класса объектов хранящихся в коллекции.
     * @return string имя класса с объектами которого работает конкретная реализация коллекции.
     */
    abstract protected function getItemClass(): string;

    /**
     * @return string Description
     */
    abstract protected function getItemName(): string;

    abstract protected function getIndex($item);
}
