<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnit;

use Webmozart\Assert\Assert;
use RobotE13\StorageAccounting\Entities\Id;

/**
 * Storage item
 *
 * Represents a common storage item containing information
 * about all of possible SKUs of this thing.
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageItem
{

    /**
     * @var Id
     */
    private $uid;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $brief;

    /**
     *
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $description;

    /**
     * @var StorageUnitsCollection
     */
    private $variants;

    /**
     * @var string идентификатор варианта артикула, который будет вариантом товара по умолчанию.
     */
    private $defaultVariant;

    /**
     *
     * @param Id $uid
     * @param string $title
     * @param string $slug
     * @param string $description
     * @param StorageUnit[] $variants
     * @param string $defaultVariant
     */
    public function __construct(Id $uid, string $title, string $slug,
            string $description, array $variants, string $defaultVariant = null, string $brief = '')
    {
//        Assert::allNotEmpty(compact('title','slug','variants'));
        $this->uid = $uid;
        $this->updateAttribute('title', $title);
        $this->updateAttribute('slug', $slug);
        $this->updateAttribute('description', $description);
        $this->updateAttribute('brief', $brief);
        $this->variants = new StorageUnitsCollection($variants);
        $this->defaultVariant = $defaultVariant ?? $variants[0]->getSkuNumber();
    }

    public function __clone()
    {
        $this->variants = clone $this->variants;
    }

    public function getUid(): Id
    {
        return $this->uid;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * Getter for brief description of the product.
     * @return string
     */
    public function getBrief(): string
    {
        return $this->brief;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     *
     * @return StorageUnitsCollection
     */
    public function getVariants(): StorageUnitsCollection
    {
        return $this->variants;
    }

    public function getDefaultVariant(): StorageUnit
    {
        return $this->variants->get($this->defaultVariant);
    }

    public function addVariant(StorageUnit $storageUnit)
    {
        $this->variants->add($storageUnit);
    }

    /**
     * Убрать вариант.
     * @param string $index строка содержащая SKU номер удаляемого варианта товара
     * @return StorageUnit
     */
    public function removeVariant($index): StorageUnit
    {
        if ($index === $this->defaultVariant)
        {
            throw new \DomainException("Can not remove default SKU `{$index}` ");
        }
        return $this->variants->remove($index);
    }

    /**
     *
     * @param type $attribute
     * @param type $value
     * @throws
     */
    public function updateAttribute($attribute, $value)
    {
        $allowed = $this->attributes();
        Assert::inArray($attribute, $allowed);
        $updaterClass = 'RobotE13\\StorageAccounting\\Entities\\StorageUnit\\Updater\\' . ucfirst($attribute) . 'Updater';

        /* @var $updater \RobotE13\StorageAccounting\Entities\UpdateStrategy */
        $updater = new $updaterClass();
        $updater->execute($value);
        $this->{$attribute} = $value;
    }

    /**
     *
     */
    private function attributes()
    {
        return ['title', 'slug', 'description', 'brief'];
    }

}
