<?php

/**
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnit\Updater;

/**
 * Description of PreviewUpdater
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class PreviewUpdater implements \RobotE13\StorageAccounting\Entities\UpdateStrategy
{
    public function execute($value)
    {
        \Webmozart\Assert\Assert::notEmpty($value);
        return;
    }

}
