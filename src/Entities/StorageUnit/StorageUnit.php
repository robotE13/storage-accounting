<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnit;

use Webmozart\Assert\Assert;
use RobotE13\StorageAccounting\Entities\Accountable;
use RobotE13\StorageAccounting\Entities\StorageUnitType\AbstractUnitType;

/**
 * Description of StorageUnit
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageUnit implements Accountable
{

    const STATUS_ACTIVE = 1;

    /**
     *
     * @var string
     */
    private $skuNumber;

    /**
     * @var string
     */
    private $title;

    /**
     * @var AbstractUnitType
     */
    private $skuType;

    /**
     *
     * @var array
     */
    private $characteristics;

    /**
     * Url to preview file.
     * @var string
     */
    private $preview;

    /**
     * @var string
     */
    private $price;

    /**
     * @var int
     */
    private $status;

    /**
     * @var int
     */
    private $availableQuantity;

    /**
     *
     * @param string $skuNumber unique storage keeping unit number
     * @param string $title unit title
     * @param AbstractUnitType $skuType
     * @param array $characteristics
     * @param string $preview url to preview image
     * @param string $price
     * @param int $availableQuantity
     */
    public function __construct(string $skuNumber, string $title, AbstractUnitType $skuType, int $status,
            array $characteristics = [], string $preview = '', float $price = 0.00, int $availableQuantity = -1)
    {
        Assert::inArray($status, [0, 1]);
        $this->skuNumber = $skuNumber;
        $this->title = $title;
        $this->skuType = $skuType;
        $this->status = $status;
        $this->characteristics = $characteristics;
        $this->preview = $preview;
        $this->price = $price;
        $this->availableQuantity = $availableQuantity;
    }

    public function isActive()
    {
        return $this->status === static::STATUS_ACTIVE;
    }

    public function enable()
    {
        $this->status = self::STATUS_ACTIVE;
    }
    public function disable()
    {
        $this->status = 0;
    }

    public function getSkuNumber(): string
    {
        return $this->skuNumber;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getSkuType(): AbstractUnitType
    {
        return $this->skuType;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getPreview(): string
    {
        return $this->preview;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getAvailableQuantity(): int
    {
        return $this->availableQuantity;
    }

    public function getMeasureUnit(): \RobotE13\StorageAccounting\Entities\MeasureUnit
    {
        return $this->skuType->getMeasureUnit();
    }

    /**
     *
     * @param string $attribute
     * @param mixed $value
     * @throws
     */
    public function updateAttribute(string $attribute, $value)
    {
        $allowed = $this->attributes();
        $updaterClass = 'RobotE13\\StorageAccounting\\Entities\\StorageUnit\\Updater\\' . ucfirst($attribute) . 'Updater';

        /* @var $updater \RobotE13\StorageAccounting\Entities\UpdateStrategy */
        if (class_exists($updaterClass))
        {
            Assert::inArray($attribute, $allowed);
            $updater = new $updaterClass();
            $updater->execute($value);
            $this->{$attribute} = $value;
        } else
        {
            $this->characteristics[$attribute] = $value;
        }
    }

    /**
     * Returns characteristic value
     * @param string $name
     * @return mixed
     * @throws \InvalidArgumentException
     */
    public function getCharacteristic($name)
    {
        Assert::keyExists($this->characteristics, $name, 'Unit has not characteristic ' . $name);
        return $this->characteristics[$name];
    }

    public function getCharacteristics(): array
    {
        return $this->characteristics;
    }

    /**
     *
     */
    private function attributes()
    {
        return array_merge(
                array_map(fn($parameter) => $parameter->getName(), $this->skuType->getParameters()),
                ['title', 'price', 'preview']
        );
    }

}
