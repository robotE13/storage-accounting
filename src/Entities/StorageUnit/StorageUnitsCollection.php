<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnit;

/**
 * Description of StorageUnitsCollection
 *
 * @method StorageUnit get(string $uid) Description
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class StorageUnitsCollection extends \RobotE13\StorageAccounting\Entities\Collection
{

    public function getActive()
    {
        /* @var $unit StorageUnit */
        return array_filter($this->getAll(), fn($unit) => $unit->isActive());
    }

    /**
     * @param StorageUnit $item
     */
    protected function getIndex($item)
    {
        return $item->getSkuNumber();
    }

    protected function getItemClass(): string
    {
        return StorageUnit::class;
    }

    protected function getItemName(): string
    {
        return "Storage unit";
    }

}
