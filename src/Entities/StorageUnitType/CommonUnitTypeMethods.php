<?php

/*
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnitType;

use RobotE13\StorageAccounting\Entities\MeasureUnit;

/**
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
trait CommonUnitTypeMethods
{

    /**
     * @var ParametersCollection
     */
    private $parameters;

    /**
     * @var MeasureUnit
     */
    private $measureUnit;

    public function getParameters(): array
    {
        return $this->parameters->getAll();
    }

    public function getMeasureUnit(): MeasureUnit
    {
        return $this->measureUnit;
    }

    public function hasParameter(string $parameter): bool
    {
        return $this->parameters->has($parameter);
    }

}
