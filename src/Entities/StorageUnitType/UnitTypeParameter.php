<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnitType;

use Webmozart\Assert\Assert;

/**
 * Description of Parameter
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class UnitTypeParameter
{

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $valueType;

    public function __construct(string $name, string $valueType)
    {
        Assert::regex($name,'/^[a-z]{2,}$/', 'Only lowercase latin characters allowed');
        $this->name = $name;
        $this->valueType = $valueType;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValueType(): string
    {
        return $this->valueType;
    }

}
