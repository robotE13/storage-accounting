<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnitType;

/**
 * Description of ParametersCollection
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
final class ParametersCollection extends \RobotE13\StorageAccounting\Entities\Collection
{

    /**
     *
     * @param UnitTypeParameter $item
     */
    protected function getIndex($item)
    {
        return $item->getName();
    }

    protected function getItemClass(): string
    {
        return UnitTypeParameter::class;
    }

    protected function getItemName(): string
    {
        return 'Parameter';
    }

}
