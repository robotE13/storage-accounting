<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnitType;

/**
 * Description of PredefinedUnitType
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
class PredefinedUnitType extends AbstractUnitType
{

    use CommonUnitTypeMethods;

    const TYPE_NAME = "Default";
    const TYPE_PARAMETERS = [
        ['name' => 'volume', 'valueType' => 'string']
    ];
    const TYPE_MEASURE_UNIT_NAME = 'pices';
    const TYPE_MEASURE_UNIT_SHORT_NAME = 'pcs';

    final public function __construct()
    {
        $parameters = array_map(fn($item) => new UnitTypeParameter($item['name'], $item['valueType']), static::TYPE_PARAMETERS);
        $this->parameters = new ParametersCollection($parameters);
        $this->measureUnit = new \RobotE13\StorageAccounting\Entities\MeasureUnit(static::TYPE_MEASURE_UNIT_NAME, static::TYPE_MEASURE_UNIT_SHORT_NAME);
    }

    public function getName(): string
    {
        return static::TYPE_NAME;
    }

}
