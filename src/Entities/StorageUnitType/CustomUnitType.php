<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnitType;

use Webmozart\Assert\Assert;
use RobotE13\StorageAccounting\Entities\{
    DynamicDataModel,
    MeasureUnit
};

/**
 * Description of Stortage Unit
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
final class CustomUnitType extends AbstractUnitType implements DynamicDataModel
{

    use CommonUnitTypeMethods;

    /**
     * Unit type name.
     * For example: Book, Service, Car e t.c.
     * @var string
     */
    private $name;

    public function __construct(string $name, array $parameters, MeasureUnit $measureUnit)
    {
        $this->name = $name;
        $this->parameters = new ParametersCollection($parameters);
        $this->measureUnit = $measureUnit;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function addParameter($parameter)
    {
        $this->parameters->add($parameter);
    }

    public function removeParameter($name)
    {
        $this->parameters->remove($name);
    }

}
