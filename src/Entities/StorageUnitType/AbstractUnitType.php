<?php

/**
 * This file is part of the storage-accounting.
 *
 * Copyright 2020 Evgenii Dudal <wolfstrace@gmail.com>.
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 * @package storage-accounting
 */

namespace RobotE13\StorageAccounting\Entities\StorageUnitType;

use RobotE13\StorageAccounting\Entities\{PluggableType,Measurable};

/**
 * Description of AbstractUnitType
 *
 * @author Evgenii Dudal <wolfstrace@gmail.com>
 */
abstract class AbstractUnitType implements PluggableType, Measurable
{
    
}
